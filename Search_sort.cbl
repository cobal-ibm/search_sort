       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Search_sort.


       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 

           SELECT 100-INPUT-FILE ASSIGN TO "STUDENT_INPUT.DAT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-INPUT-FILE-STATUS.

           SELECT 200-OUTPUT-FILE ASSIGN TO "STUDENT_SORT.RPT"
           ORGANIZATION IS LINE SEQUENTIAL
           FILE STATUS IS WS-OUTPUT-FILE-STATUS.


       DATA DIVISION. 
       FILE SECTION. 

       FD  100-INPUT-FILE 
           BLOCK CONTAINS 0 RECORDS.

       01  100-INPUT-RECORDS.
           05 100-STU-ID           PIC X(4).
           05 FILLER               PIC X.
           05 100-STU-NAME         PIC X(20).

       FD  200-OUTPUT-FILE 
           BLOCK CONTAINS 0 RECORDS.

       01  200-OUTPUT-RECORDS      PIC X(80).
          
       WORKING-STORAGE SECTION.

       01  WS-INPUT-FILE-STATUS    PIC X(2).
           88 FILE-OK              VALUE "00".
           88 FILE-AT-END          VALUE "10".

       01  WS-OUTPUT-FILE-STATUS   PIC X(2).
           88 FILE-OK              VALUE "00".
           88 FILE-AT-END          VALUE "10".    
       
       01  WS-CAL.
           05 WS-INPUT-COUNT       PIC 9(5) VALUE ZERO.
           05 WS-SUB               PIC 9(5) VALUE ZERO.

       01  WS-ARRAY-STU OCCURS 7 TIMES 
               ASCENDING KEY IS WSA-STU-ID  INDEXED BY WSA-IDX.
           05 WSA-STU-ID           PIC X(4).
           05 FILLER               PIC X.
           05 WSA-STU-NAME         PIC X(20).
       

       PROCEDURE DIVISION.

       0000-MAIN.
           PERFORM 1000-INITIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
           PERFORM 3000-END     THRU 3000-EXIT

           GOBACK
           .

       1000-INITIAL.
           OPEN INPUT 100-INPUT-FILE 
           OPEN OUTPUT 200-OUTPUT-FILE 
           PERFORM 8000-READ THRU 8000-EXIT
           
           PERFORM 4000-LOAD THRU 4000-EXIT
           .
           

       1000-EXIT.
           EXIT.


       2000-PROCESS.
      *    DISPLAY WSA-STU-ID " " WSA-STU-NAME 
           PERFORM 2100-PRINT-ARR-STU THRU 2100-EXIT
           SET WSA-IDX TO 1
           SEARCH WS-ARRAY-STU 
              AT END DISPLAY "NOT FOUND !!"
              WHEN WSA-STU-ID(WSA-IDX) = "9056" 
              DISPLAY "FOUNDED : "WS-ARRAY-STU(WSA-IDX) "AT " WSA-IDX 
           END-SEARCH
           
           SET WSA-IDX TO 1
           SEARCH WS-ARRAY-STU 
              AT END DISPLAY "NOT FOUND !!"
              WHEN WSA-STU-NAME(WSA-IDX) = "James Smith" 
              DISPLAY "FOUNDED : "WS-ARRAY-STU(WSA-IDX) "AT " WSA-IDX 
           END-SEARCH

           SORT WS-ARRAY-STU ASCENDING WSA-STU-ID 
           DISPLAY "------Sort by ID-----"
           PERFORM 2100-PRINT-ARR-STU THRU 2100-EXIT 
           
          
           
           SET WSA-IDX TO 1
           SEARCH ALL WS-ARRAY-STU 
              AT END DISPLAY "NOT FOUND !!"
              WHEN WSA-STU-ID(WSA-IDX) = "9056" 
              DISPLAY "FOUNDED : "WS-ARRAY-STU(WSA-IDX) "AT " WSA-IDX 
           END-SEARCH

           
           PERFORM VARYING WS-SUB FROM 1 BY 1 
              UNTIL WS-SUB > WS-INPUT-COUNT 
              MOVE WS-ARRAY-STU(WS-SUB) TO 200-OUTPUT-RECORDS
              PERFORM 7000-WRITE THRU 7000-EXIT 
           END-PERFORM
           .
       2000-EXIT.
           EXIT.

       2100-PRINT-ARR-STU.
           PERFORM VARYING WS-SUB 
              FROM 1 BY 1 UNTIL WS-SUB > WS-INPUT-COUNT 
              DISPLAY WS-ARRAY-STU(WS-SUB)
           END-PERFORM
           .
        
       2100-EXIT.
           EXIT.

       3000-END.
           CLOSE 100-INPUT-FILE 200-OUTPUT-FILE 
           DISPLAY "Read Input Count : " WS-INPUT-COUNT 
           .

       3000-EXIT.
           EXIT.
       
       4000-LOAD.
           PERFORM VARYING WS-SUB 
              FROM 1 BY 1 UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS 
              OR WS-SUB > WS-INPUT-COUNT
              MOVE 100-INPUT-RECORDS TO WS-ARRAY-STU(WS-SUB) 
              PERFORM 8000-READ THRU 8000-EXIT
           END-PERFORM
           .

       4000-EXIT.
           EXIT.
       

       7000-WRITE.
           WRITE 200-OUTPUT-RECORDS
           .

       7000-EXIT.
           EXIT.

       8000-READ.
           READ 100-INPUT-FILE 
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD 1 TO WS-INPUT-COUNT
           END-IF 
           

           .

       8000-EXIT.
           EXIT.